// Pinout del Shield Multifuncional 9 en 1:
// LED1 (azul): D13
// LED2 (rojo): D12
// SW1 (botón): D2
// SW2 (botón): D3
// DHT11 (sensor de temperatura y humedad): D4
// Zumbador: D5
// Receptor de infrarrojos: D6
// LED RGB: D9, D10 y D11
// Potenciómetro: A0
// LDR (sensor de luz): A1
// LM35 (sensor de temperatura): A2
// 2 puertos digitales: D7 y D8
// 1 puerto analógico: A3
// Puerto I2C: SDA A4, SCL A5
// Puerto UART: TXD y RXD

int pot;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // configura el serial
  Serial.println("hola jashi");  // envia por el serial la cadena
  // I/O configs
  // LED1 (azul): D13
  // LED2 (rojo): D12
  pinMode(12,OUTPUT); // configura la salida de al tarjeta led rojo  I/O
  pinMode(13,OUTPUT); // configura la salida de al tarjeta led azul I/O
  pinMode(9,OUTPUT); // configura la salida de al tarjeta led azul I/O
  pinMode(10,OUTPUT); // configura la salida de al tarjeta led azul I/O
  pinMode(11,OUTPUT); // configura la salida de al tarjeta led azul I/O

  pinMode(5,OUTPUT); // configura la salida BUZ

  pinMode(2,INPUT); // configura la entrada  SW1 (botón): D2
  pinMode(3,INPUT); // configura la entrada SW2 (botón): D3

  digitalWrite(12,LOW); // pone la salida en alto
  digitalWrite(13,HIGH); // pone la salida en alto

  digitalWrite(9,LOW); // pone la salida en alto
  digitalWrite(10,HIGH); // pone la salida en alto
  digitalWrite(11,LOW); // pone la salida en alto
  
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(500);
  digitalWrite(12,LOW); // pone la salida en alto
  digitalWrite(13,HIGH); // pone la salida en alto
  delay(500);
  digitalWrite(12,HIGH); // pone la salida en alto
  digitalWrite(13,LOW); // pone la salida en alto
  digitalWrite(9,LOW); // pone la salida en alto
  delay(500);    
  digitalWrite(9,!digitalRead(2)); // pone la salida en alto
  digitalWrite(10,!digitalRead(3)); // pone la salida en alto
  digitalWrite(11,LOW); // pone la salida en alto
  // Serial.println(" ");
  // Serial.print(digitalRead(2));  // envia por el serial la cadena  
  // Serial.print(" ");
  // Serial.print(digitalRead(3));  // envia por el serial la cadena
  Serial.print("LM35 ");  // envia por el serial la cadena
  Serial.print(analogRead(A2));  // envia por el serial la cadena

  pot = analogRead(A0);
  Serial.print(" POT ");  // envia por el serial la cadena
  Serial.println(pot);  // envia por el serial la cadena

  // sumbar > POT > 500
  if (pot > 500) {
    analogWrite(5,100);
    Serial.print(" BUZZING ");
  }
  else {
    digitalWrite(5,LOW);
  }

}
