<h1 align="center">Hi 👋, I'm Enthusiastic android developer</h1>
<h3 align="center">A passionate frontend developer from Mexico</h3>

- 🔭 I’m currently working on **Aruduino multishield training**

<h3 align="left">Connect with me:</h3>
<p align="left">
</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://www.arduino.cc/" target="_blank" rel="noreferrer"> <img src="https://cdn.worldvectorlogo.com/logos/arduino-1.svg" alt="arduino" width="40" height="40"/> </a> <a href="https://www.cprogramming.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a> <a href="https://www.w3schools.com/cpp/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/> </a> </p>


<h2>Playground aurduino</h2>
https://wokwi.com

<h2>Readme.md Generator</h2>
https://rahuldkjain.github.io/gh-profile-readme-generator/

<h2>Good practices</h2>
https://www.theatreofnoise.com/2017/05/arduino-ide-best-practices-and-gotchas.html


